#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 16:40:59 2018

@author: rachid
"""

# MongoDB conf
mongoConf = {'host':'35.187.187.39',
             'port':27017,
             'db':'wiztracker'}

# RabbitMQ conf
rabbitConf = {'host':'proxy.wiztracker.net',
              'port':'5672',
              'user':'admin',
              'pass':'as2Badmin4U'}

# Models paths (Keras + Tensorflow)
modelPath = {'M1':'/home/rachid/Notebooks/demo-estimation-v1/models/M1/pretrained_models/weights.18-4.06.hdf5',
             'M2_age':'/home/rachid/Notebooks/demo-estimation-v1/models/M2/pretrained_models/22801/checkpoint-14999',
             'M2_gender':'/home/rachid/Notebooks/demo-estimation-v1/models/M2/pretrained_models/21936/checkpoint-14999'}