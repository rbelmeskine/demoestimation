#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 10:46:22 2018

@author: rachid
"""
from pymongo import MongoClient
#from pprint import pprint
import pandas as pd
from conf import mongoConf
from datetime import datetime

class MongoInterface:
    def __init__(self):
        self.client = MongoClient(mongoConf['host'], mongoConf['port'])
        self.db = self.client[mongoConf['db']]
    
    def updateInstagramLikers(self, df):
        bulk = self.db['instagram_likers'].initialize_unordered_bulk_op()
        for idx, row in df.iterrows():
            #print(row)
            if(row['trancheAge'] != 'UNK'):
                bulk.find({'_id': row['id']}).update({'$set': {'gender': row['gender'], 'demography': row['trancheAge'], 'last_demo_computed_date': datetime.now()}})
                continue
            if(row['gender'] != 'UNK'):
                bulk.find({'_id': row['id']}).update({'$set': {'gender': row['gender'], 'last_demo_computed_date': datetime.now()}})
                continue
            bulk.find({'_id': row['id']}).update({'$set': {'last_demo_computed_date': datetime.now()}})
        bulk.execute()
    
    def getInstagramLikersWithLimit(self, processedGender=False, limit=1000):
        collection = self.db['instagram_likers']
        queryTwo = list(collection.find({"gender":{'$exists':processedGender}}).limit(limit))
        df =  pd.DataFrame(queryTwo)
        return df
    
    def getInstagramLikersByIds(self, ids):
        collection = self.db['instagram_likers']
        queryTwo = list(collection.find({"_id":{'$in':ids}}))
        df =  pd.DataFrame(queryTwo)
        return df
    
    def getInstagramLikersIds(self, processedGender=False, limit=150000):
        collection = self.db['instagram_likers']
        queryTwo = list(collection.find({"$and":[{'last_demo_computed_date':{'$exists':processedGender}}, {'profile_pic_url':{'$ne':None}}]}).limit(limit))
        df =  pd.DataFrame(queryTwo)
        df = df['_id']
        return df
        
    def close(self):
        self.client.close()
