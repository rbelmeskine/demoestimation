#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 09:05:27 2018

@author: rachid
"""

import pika
import json
from dao.MongoInterface import MongoInterface

from conf import rabbitConf

from models.Main import executeAllModels

import threading
import time

class Consumer:
    def __init__(self):
        self.rabbitConf = rabbitConf
        self.queueConf = {'name':'demo-estimator'}
        self.mongoInterface = MongoInterface()
        
    def listen(self):
        #try:
        credentials = pika.PlainCredentials(self.rabbitConf['user'], self.rabbitConf['pass'])
        connection = pika.BlockingConnection(pika.ConnectionParameters(self.rabbitConf['host'],
                                                                       self.rabbitConf['port'],
                                                                       '/',
                                                                       credentials))
        
        processThread = threading.Thread(target=self.keepConnection, args=(connection,))
        processThread.start()
        
        channel = connection.channel()
        channel.queue_declare( self.queueConf['name'], # queue name 
                        durable=True)  # create automatically on reboot
        channel.basic_qos(prefetch_count=1) # don't dispatch a new message to a worker until it has processed and acknowledged the previous one
        channel.basic_consume(self.consumeMsg,
                        queue=self.queueConf['name'],
                        no_ack=False)  #if set to false, message will stay on server till chan.basic_ack() called
        channel.start_consuming()
        channel.close()
        connection.close()
        self.mongoInterface.close()
        '''except Exception:
            print("Something went wrong : ", Exception)'''
            
    def consumeMsg(self, ch, method, properties, body):
        '''
        consumes a message that contain instagram profiles ids
        genrates gender and demo for each profile and store them in mongo DB
        '''
        
        print('NEW MSG...')
        
        msg = json.loads(body)
        source = msg['source']
        ids = msg['ids']
        
        print('SOURCE : ', source,', ids : ', ids)
        
        df = self.mongoInterface.getInstagramLikersByIds(ids)
        
        print(str(len(df)))
        
        if(len(df) > 0):
            df['id'] = df['_id']
            df['name'] = df['full_name']
            df = df[['id', 'name', 'profile_pic_url']]
            df = executeAllModels(df)
            self.mongoInterface.updateInstagramLikers(df)
        
        print('FINISH...')
        ch.basic_ack(delivery_tag = method.delivery_tag)
        
    def keepConnection(self, connection):
        '''
        allows to keep the connection alive (to be executed in a thread)
        '''
        while(True):
            print('KEEP CONNECTION...')
            connection.process_data_events()
            time.sleep(100)
        
consumer = Consumer()
consumer.listen()