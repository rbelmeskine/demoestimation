# -*- coding: utf-8 -*-

import sys

from models.ModelFN import ModelFN
from models.ModelM1 import ModelM1
from models.ModelM2 import ModelM2
from models.DataFrameContainer import DataFrameContainer
from models.Main import executeAllModels

if(len(sys.argv) == 2):

    dfContainer = DataFrameContainer()
    
    df = dfContainer.getDataFrameFromInsta(sys.argv[1])
    
    df_ = df.copy()
    df_ = df_.drop_duplicates(subset='id', keep='last')
    
    print('LIKERS LIST LEN : ', str(len(df_)))
    
    #df_ = df_[:100].copy()
    
    dfResult = executeAllModels(df_)
    
    def genderFromFN(df):
        l = len(df[df['genderFN'] != 'UNK'])
        dic = df[df['genderFN'] != 'UNK']['genderFN'].value_counts().to_dict()
        res = dict()
        genderList = ['F', 'M']
        for key in genderList:
            if(key in dic.keys()):
                res[key] = (dic[key] / l) * 100
            else:
                res[key] = 0
        return res
    
    genderFN = genderFromFN(dfResult)
    
    def getTranches(df):
        l = len(df[df['trancheAgeGender'] != 'UNK'])
        dic = df['trancheAgeGender'].value_counts().to_dict()
        res = dict()
        tranchesList = ['F 13-17', 'F 18-24', 'F 25-34', 'F 35-44', 'F 45-54', 'F 55-64', 'F 65+', 'M 13-17', 'M 18-24', 'M 25-34', 'M 35-44', 'M 45-54', 'M 55-64', 'M 65+']
        for key in tranchesList:
            if(key in dic.keys()):
                res[key] = (dic[key] / l) * 100
            else:
                res[key] = 0
        return res
    
    dic1 = getTranches(dfResult)
    
    def regTranches(dic):
        def regtanches(v0, l):
            suml = sum(l)
            p = suml + v0
            if(suml != 0):
                f = p / sum(l)
            else:
                f = 0
            for i in range(len(l)):
                l[i] = l[i] * f
            return l
        l1 = regtanches(list(dic.values())[0], list(dic.values())[1:7])
        l2 = regtanches(list(dic.values())[7], list(dic.values())[8:])
        return {'F 18-24':l1[0], 'F 25-34':l1[1], 'F 35-44':l1[2], 'F 45-54':l1[3], 'F 55-64':l1[4], 'F 65+':l1[5],
           'M 18-24':l2[0], 'M 25-34':l2[1], 'M 35-44':l2[2], 'M 45-54':l2[3], 'M 55-64':l2[4], 'M 65+':l2[5]}, {'F':sum(l1),'M':sum(l2)}
    
    dic2, genderImage = regTranches(dic1)
    
    def regTranchesFN(dic, genderFN):
        lf = list(dic.values())[:6]
        lm = list(dic.values())[6:]
        genderImage = {'F':sum(lf), 'M':sum(lm)}
        facF = genderFN['F'] / genderImage['F']
        facM = genderFN['M'] / genderImage['M']
        lf = [e * facF for e in lf]
        lm = [e * facM for e in lm]
        return {'F 18-24':lf[0], 'F 25-34':lf[1], 'F 35-44':lf[2], 'F 45-54':lf[3], 'F 55-64':lf[4], 'F 65+':lf[5],
           'M 18-24':lm[0], 'M 25-34':lm[1], 'M 35-44':lm[2], 'M 45-54':lm[3], 'M 55-64':lm[4], 'M 65+':lm[5]}
        
    dic3 = regTranchesFN(dic2, genderFN)
    
    print('MODEL :')
    print(dic1)
    print('MODEL + REG:')
    print(genderImage)
    print(dic2)
    print('MODEL + REG + FN :')
    print(genderFN)
    print(dic3)
    
else:
    print('ARGUMENT PROBELM')