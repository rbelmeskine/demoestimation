#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 10:41:58 2018

@author: rachid
"""

import pika
import json

from conf import rabbitConf
from dao.MongoInterface import MongoInterface

class Producer:
    def __init__(self):
        self.rabbitConf = rabbitConf
        self.queueConf = {'name':'demo-estimator'}
        self.mongoInterface = MongoInterface()
        self.BATCH_SIZE = 100
        
    def produce(self, source='instagram'):
        '''
        produces messages that contain instagram profiles ids
        each message contains 100 ids
        '''
        
        ids = self.mongoInterface.getInstagramLikersIds()
        
        l = len(ids)
        
        print('NUMBER OF MESSAGES : ', str(l))
        
        if(l==0):
            print('EMPTY IDS LIST...')
            return
         
        msgs = []
        for i in range(int(l / self.BATCH_SIZE + 1)):
            
            start = i * self.BATCH_SIZE
            end = (i + 1) * self.BATCH_SIZE
            
            if(end > l):
                end = l
            
            if(start == end):
                break
            
            msg = {'source':source,
                  'ids':ids[start:end].tolist()}
        
            msgs.append(msg)
        
        print('NUMBER OF MESSAGES : ', str(len(msgs)))
        
        #try:
        credentials = pika.PlainCredentials(self.rabbitConf['user'], self.rabbitConf['pass'])
        connection = pika.BlockingConnection(pika.ConnectionParameters(self.rabbitConf['host'],
                                                                       self.rabbitConf['port'],
                                                                       '/',
                                                                       credentials))
        channel = connection.channel()
        channel.queue_declare(self.queueConf['name'], # queue name 
                        durable=True)  # create automatically on reboot
                        #exclusive=False, #  we dont want it to be only accesible by queue creator
                        #auto_delete=False) # we dont want the queue to be deleted when last consumer detached
        for msg in msgs:
            channel.basic_publish(exchange='',
                            routing_key=self.queueConf['name'], 
                            body=json.dumps(msg),
                            properties=pika.BasicProperties(
                                    delivery_mode = 2 # make message persistent
                                    )
                            ) # if set to false, message will stay on server till chan.basic_ack() called

        channel.close()
        connection.close()
        #except:
            #print('RABBITMQ ERROR...')
        
producer = Producer()
producer.produce()