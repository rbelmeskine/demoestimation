#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 17:28:44 2018

@author: rachid
"""

from models.M1.wide_resnet import WideResNet
import dlib
import cv2
import keras
import skimage.io as io
import numpy as np
import pandas as pd

from conf import modelPath


class ModelM1:
    
    def __init__(self, unknownInterval=[0.04, 0.9], decisionThreshold=0.5):
        keras.backend.clear_session()
        self.TMP_DIR = '/tmp/wd_img'
        self.img_size = 64
        self.model = WideResNet(self.img_size, depth=16, k=8)()
        print(modelPath['M1'])
        self.model.load_weights(modelPath['M1'])
        self.detector = dlib.get_frontal_face_detector()
        self.unknownInterval = unknownInterval
        self.decisionThreshold = decisionThreshold
        self.i = 0
    
    def execute(self, df):
        return pd.concat([df, df['id'].apply(self.predictGenderAndAgeLambda)], axis=1)
    
    def predictGenderAndAgeLambda(self, id):
        res = self.getPredictions(self.TMP_DIR + '/' + str(id))
        return pd.Series({'predGenderM1':res[0], 'predGenderM1Prob':max(res[1]), 'predAgeM1':res[2]})
    
    def getPredictions(self, fileName):
        print(str(self.i) + ' : ' + fileName)
        self.i += 1
        
        try:
            faces, input_img, detected = self.getFaces(fileName)
        except:
            return 'NOP', [0], None, None
        
        if(faces is None or len(detected) == 0 or len(detected) > 1):
            return 'NOF', [len(detected)], None, None
        predicted_genders, predicted_ages = self.getPredictionsFromFaces(faces)
        
        if(predicted_genders[0][0] > self.unknownInterval[0] and predicted_genders[0][0] < self.unknownInterval[1]):
            return 'UNK', predicted_genders[0], predicted_ages, None
        
        if(predicted_genders[0][0] > self.decisionThreshold):
            return 'F', predicted_genders[0], predicted_ages
        else:
            return 'M', predicted_genders[0], predicted_ages
    
    def getFaces(self, fileName):
        try:
            img = io.imread(fileName)
        except:
            print('Error : 404')
            return None, None, []
        input_img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        #plt.imshow(cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB))
        detected = self.detector(input_img, 1)
        #print(len(detected))
        if(len(detected)<1):
            return None, None, detected
        faces = np.empty((len(detected), self.img_size, self.img_size, 3))
        img_h, img_w, _ = np.shape(input_img)
        for i, d in enumerate(detected):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            xw1 = max(int(x1 - 0.4 * w), 0)
            yw1 = max(int(y1 - 0.4 * h), 0)
            xw2 = min(int(x2 + 0.4 * w), img_w - 1)
            yw2 = min(int(y2 + 0.4 * h), img_h - 1)
            cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
            cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i,:,:,:] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (self.img_size, self.img_size))
        return faces, input_img, detected
    
    def getPredictionsFromFaces(self, faces):
        results = self.model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()
        return predicted_genders, predicted_ages