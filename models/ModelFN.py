#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 17:25:33 2018

@author: rachid
"""
import gender_guesser.detector as gender
import unicodedata
import re

class ModelFN:
    def __init__(self):
        self.detector = gender.Detector()
        
    def getFirstName(self, fullName):
        fullName = fullName.replace('.', ' ')
        fullName = fullName.replace('_', ' ')
        fullName = unicodedata.normalize('NFKD', fullName).encode('ASCII', 'ignore')
        fullName = re.sub('[^A-Za-z]', ' ', fullName.decode())
        fullNameTab = fullName.split(' ')
        if(len(fullNameTab) == 0):
            return ''
        return fullNameTab[0].title()
    
    def execute(self, df):
        df['first_name'] = df['name'].apply(self.getFirstName)
        df['genderFN'] = df['first_name'].apply(lambda fn: self.detector.get_gender(fn))
        df.loc[(df['genderFN'] == 'male'), 'genderFN'] = 'M'
        df.loc[(df['genderFN'] == 'female'), 'genderFN'] = 'F'
        df.loc[(df['genderFN'] == 'unknown') | (df['genderFN'] == 'andy'), 'genderFN'] = 'UNK'
        df.loc[(df['genderFN'] == 'mostly_male') | (df['genderFN'] == 'mostly_female'), 'genderFN'] = 'UNK'
        return df
    
    def adjust(self, df):
        df['predAgeM1'] = [[0]] * len(df)
        df['predGenderM1'] = 'UNK'
        df['predGenderM1Prob'] = 0
        df['predAgeM2'] = 'UNK'
        df['predAgeM2Prob'] = 0
        df['predAgeM2Second'] = 'UNK'
        df['predAgeM2SecondProb'] = 0
        df['predGenderM2'] = 'UNK'
        df['predGenderM2Prob'] = 0
        df['trancheAge'] = 'UNK'
        df['gender'] = 'UNK'
        df['trancheAgeGender'] = 'UNK'
        return df