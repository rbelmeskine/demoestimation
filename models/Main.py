#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 15:49:18 2018

@author: rachid
"""

import glob
import os
import urllib
import pandas as pd
from models.ModelFN import ModelFN
from models.ModelM1 import ModelM1
from models.ModelM2 import ModelM2

def executeAllModels(df):
    
    TMP_PATH = '/tmp/wd_img'
    modelFN = ModelFN()
    modelM1 = ModelM1()
    modelM2 = ModelM2()
    
    print('DF LEN : '+ str(len(df)))
    
    df.apply(downloadImg, args=(TMP_PATH,), axis=1)
    
    dfFN = modelFN.execute(df)
    
    dfM1 = modelM1.execute(dfFN)
    
    print('LEN DFM1 : '+ str(len(dfM1)))
    
    dfM1 = dfM1[(dfM1['predAgeM1'].notnull()) & (dfM1['predGenderM1']!='UNK')].copy()    
    dfM2 = modelM2.execute(dfM1)
    
    dfFN = modelFN.adjust(dfFN)
    
    result = pd.concat([dfM2, dfFN], axis=0)
    result = result.drop_duplicates(subset='id', keep='first')
    
    def getGender(row):
        if(row['genderFN'] != 'UNK'):
            return row['genderFN']
        return row['gender']
    
    result['gender'] = result.apply(getGender, axis=1)
    
    removeFolderContent(TMP_PATH)
    
    return result

def downloadImg(row, *args):
    
    path = args[0]
    
    url = row['profile_pic_url']
    url = url.replace('/vp/', '/')
    url = url.replace('/s150x150/', '/s1080x1080/')
    
    fileName = path + '/' + str(row['id'])
    
    print('DOWNLOAD : ', url, ' => ', fileName)
    
    try:
        
        req = urllib.request.Request(url)
        response = urllib.request.urlopen(req)
        image_data = response.read()

        output = open(fileName,'wb')
        output.write(image_data)
        output.close()
    
    except:
        print('NOT FOUND')

def removeFolderContent(path):
    
    print('REMOVE : ', path + '/*')
    
    files = glob.glob(path + '/*')
    for f in files:
        os.remove(f)