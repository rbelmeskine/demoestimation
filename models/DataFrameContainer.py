#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 11:47:22 2018

@author: rachid
"""

from urllib import request
import pandas as pd
from pandas.io.json import json_normalize
import json
import glob

from bs4 import BeautifulSoup
from selenium import webdriver


class DataFrameContainer:
    def getDataFrameCombinedFromUrlsInstagram(self, urls):
        appended_dfs = []
        for url in urls:
            print(url)
            df = self.getDataFrameFromUrlInstagram(url)
            '''if(justKnownGender):
                df = self.getDFknownGender(df)'''
            appended_dfs.append(df)
        return pd.concat(appended_dfs, axis=0)
    def getDataFrameFromUrlInstagram(self, url):
        req = request.Request(url)
        r = request.urlopen(req).read()
        jsonData = json.loads(r.decode('utf-8'))
        dfAll = json_normalize(jsonData['data']['shortcode_media']['edge_liked_by']['edges'])
        df = dfAll[['node.id', 'node.full_name', 'node.profile_pic_url']].copy()
        df.columns = ['id', 'name', 'profile_pic_url']
        return df
    def getDataFrameCombinedJsonTwitter(self, jsonDataAll):
        appended_dfs = []
        for jsonData in jsonDataAll:
            df = self.getDataFrameFromJsonTwitter(jsonData)
            '''df = self.detector.generateGenderFromName(df)
            if(justKnownGender):
                df = self.getDFknownGender(df)'''
            appended_dfs.append(df)
        return pd.concat(appended_dfs, axis=0)
    def getDataFrameFromJsonTwitter(self, jsonData):
        df = json_normalize(jsonData)
        return df
    '''def generateGenderFromFirstName(df):
        return self.detector.generateGenderFromName(df)'''
    def getDFknownGender(self, oldDf, genderCol):
        df = oldDf[(oldDf[genderCol] != 'unknown') & (oldDf[genderCol] != 'andy') & (oldDf[genderCol] != 'mostly_male') & (oldDf[genderCol] != 'mostly_female')].copy()
        #df.loc[df['gender_first_name'] == 'mostly_male', 'gender_first_name'] = 'male'
        #df.loc[df['gender_first_name'] == 'mostly_female', 'gender_first_name'] = 'female'
        return df
    '''def getScore(self, df):
        value_counts = df['gender_first_name'].value_counts()
        detectedValues = value_counts['male'] + value_counts['female'] + value_counts['mostly_male'] + value_counts['mostly_female']
        allValues = sum(value_counts)
        print('detected:',detectedValues)
        print('all: ',allValues)
        return detectedValues / allValues'''
    #Instagram :  posts id in files...
    def getDataFrameFromFilePostsIds(self, fname):
        with open(fname) as f:
            content = f.readlines()
        lines = [x.strip() for x in content] 
        urls = []
        for line in lines:
            url = 'https://www.instagram.com/graphql/query/?query_id=17864450716183058&variables=%7B%22shortcode%22%3A%22'+line+'%22%2C%22first%22%3A500%7D'
            urls.append(url)
        len(urls)
        dfContainer = DataFrameContainer()
        df = dfContainer.getDataFrameCombinedFromUrlsInstagram(urls)
        return df

    def getDataFrameFromFilesPostsIds(self, path):
        allFiles = glob.glob(path)
        frame = pd.DataFrame()
        list_ = []
        for file_ in allFiles:
            print(file_)
            df = self.getDataFrameFromFilePostsIds(file_)
            list_.append(df)
        frame = pd.concat(list_)
        return frame
    def getDataFrameFromCSVFiles(self, path):
        allFiles = glob.glob(path)
        print('ok')
        frame = pd.DataFrame()
        list_ = []
        for file_ in allFiles:
            print(file_)
            df = pd.read_csv(file_, index_col=None, header=None)
            list_.append(df)
        frame = pd.concat(list_)
        #frame.columns = ['', 'id','full_name','profile_pic_url','first_name','gender_first_name']
        return frame
    
    def getTokensFromInsta(self, page):
        driver = webdriver.Chrome('/home/rachid/Bureau/chromedriver')
        driver.get(page)
        html = driver.page_source
        soup = BeautifulSoup(html, "lxml")
        tokens = []
        for div in soup.find_all('div', class_='_mck9w _gvoze _tn0ps'):
            t = div.find('a')['href']
            tokens.append(t.split('/')[2])
        return tokens

    def getDataFrameFromInsta(self, page):
        lines = self.getTokensFromInsta(page) 
        urls = []
        for line in lines:
            url = 'https://www.instagram.com/graphql/query/?query_id=17864450716183058&variables=%7B%22shortcode%22%3A%22'+line+'%22%2C%22first%22%3A500%7D'
            urls.append(url)
        len(urls)
        dfContainer = DataFrameContainer()
        df = dfContainer.getDataFrameCombinedFromUrlsInstagram(urls)
        return df
