#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 15:40:13 2018

@author: rachid
"""

import numpy as np
import pandas as pd
import tensorflow as tf
from models.M2.data import inputs
from models.M2.model import select_model, get_checkpoint
from models.M2.utils import *

from conf import modelPath

class ModelM2:
    
    def __init__(self):
        self.TMP_DIR = '/tmp/wd_img'
        self.MODEL_TYPE = 'inception'
        self.MODEL_AGE_CHECKPOINT_PATH = modelPath['M2_age']
        self.MODEL_GENDER_CHECKPOINT_PATH = modelPath['M2_gender']
        self.RESIZE_FINAL = 227
        self.MAX_BATCH_SZ = 128
        self.BATCH_DF_SIZE = 100
        self.GENDER_LIST =['M','F']
        self.AGE_LIST = ['(0, 2)','(4, 6)','(8, 12)','(15, 20)','(25, 32)','(38, 43)','(48, 53)','(60, 100)']
        self.label_list = ['(0, 2)','(4, 6)','(8, 12)','(15, 20)','(25, 32)','(38, 43)','(48, 53)','(60, 100)']
        
    def execute(self, df):
        
        print('EXECUTE M2...')
        
        if(len(df) > 0):
            df = self.predictAge(df)
            df = self.predictGender(df)
            
            df['trancheAge'] = df.apply(self.getTrancheAge, axis=1)
            df['gender'] = df.apply(self.getGender, axis=1)
            df['trancheAgeGender'] = df.apply(self.mergeGenderAndAge, axis=1)
        
        return df    
    
    def predictGender(self, df):
        
        print('PREDICT GENDER M2')
    
        label_list = self.GENDER_LIST
        nlabels = len(label_list)
        
        frames = []
        l = len(df)
        
        print('LEN : ', l)
        
        for i in range(int(l / self.BATCH_DF_SIZE + 1)):
            
            start = i * self.BATCH_DF_SIZE
            end = (i + 1) * self.BATCH_DF_SIZE
            
            if(end > l):
                end = l
            
            if(start == end):
                break
                
            print(start, ' : ', end)
            
            frame = df[start:end].copy()
        
            config = tf.ConfigProto(allow_soft_placement=True)
            tf.reset_default_graph()
            
            sess = tf.Session(config=config)

            model_fn = select_model(self.MODEL_TYPE)

            images = tf.placeholder(tf.float32, [None, self.RESIZE_FINAL, self.RESIZE_FINAL, 3])
            logits = model_fn(nlabels, images, 1, False)
            softmax_output = tf.nn.softmax(logits)

            tf.global_variables_initializer()
 
            saver = tf.train.Saver()
            saver.restore(sess, self.MODEL_GENDER_CHECKPOINT_PATH)

            self.first = True
            frame = pd.concat([frame, frame.apply(self.predictGenderLambda, args=(label_list, images, softmax_output, sess), axis=1)], axis=1)

            frames.append(frame)
            
            sess.close()
            
        df = pd.concat(frames)

        return df
    
    def predictGenderLambda(self, row, *args):
        
        if(self.first):
            print(self.first)
            self.first = False
            return pd.Series({'predGenderM2':0,'predGenderM2Prob':0})
        
        if(row['genderFN'] != 'UNK'):
            return pd.Series({'predGenderM2':row['genderFN'],'predGenderM2Prob':1})

        label_list = args[0]
        images = args[1]
        softmax_output = args[2]
        sess = args[3]
        
        id = row['id']
        fileName = self.TMP_DIR + "/" + str(id)
        
        print('M2 GENDER : ', fileName)

        #try:
        coder = ImageCoder()
        image_batch = make_multi_crop_batch(fileName, coder)
        '''except:
            print('NOT FOUND')
            return 'UNK'"'''

        batch_results = sess.run(softmax_output, feed_dict={images:sess.run(image_batch)})
        output = batch_results[0]
        batch_sz = batch_results.shape[0]

        for i in range(1, batch_sz):
            output = output + batch_results[i]

        output /= batch_sz
        best = np.argmax(output)
        result = (label_list[best], output[best])

        return pd.Series({'predGenderM2':result[0],'predGenderM2Prob':result[1]})
    
    def predictAge(self, df):
        
        print('PREDICT AGE M2')
        
        label_list = self.AGE_LIST
        nlabels = len(label_list)
        
        frames = []
        l = len(df)
        
        print('LEN DF : ', l)
        
        for i in range(int(l / self.BATCH_DF_SIZE + 1)):
            
            start = i * self.BATCH_DF_SIZE
            end = (i + 1) * self.BATCH_DF_SIZE
            
            if(end > l):
                end = l
            
            if(start == end):
                break
                
            print(start, ' : ', end)
            
            frame = df[start:end].copy()
        
            config = tf.ConfigProto(allow_soft_placement=True)
            tf.reset_default_graph()
            
            sess = tf.Session(config=config)

            model_fn = select_model(self.MODEL_TYPE)

            images = tf.placeholder(tf.float32, [None, self.RESIZE_FINAL, self.RESIZE_FINAL, 3])
            logits = model_fn(nlabels, images, 1, False)
            softmax_output = tf.nn.softmax(logits)

            tf.global_variables_initializer()

            saver = tf.train.Saver()
            saver.restore(sess, self.MODEL_AGE_CHECKPOINT_PATH)

            self.first = True
            frame = pd.concat([frame, frame.apply(self.predictAgeLambda, args=(label_list, images, softmax_output, sess), axis=1)], axis=1)

            frames.append(frame)
            
            sess.close()
        
        print('LEN FRAMES : ', str(len(frames)))
        df = pd.concat(frames)
        
        return df
    
    def predictAgeLambda(self, row, *args):
    
        if(self.first):
            print(self.first)
            self.first = False
            return pd.Series({'predAgeM2':0,'predAgeM2Prob':0,'predAgeM2Second':0,'predAgeM2SecondProb':0})
        
        label_list = args[0]
        images = args[1]
        softmax_output = args[2]
        sess = args[3]
        
        id = row['id']
        fileName = self.TMP_DIR + "/" + str(id)
        
        print('M2 AGE : ', fileName)
            
        #try:
        coder = ImageCoder()
        image_batch = make_multi_crop_batch(fileName, coder)
        '''except:
            print('NOT FOUND')
            return 'UNK'"'''
        
        batch_results = sess.run(softmax_output, feed_dict={images:sess.run(image_batch)})
        output = batch_results[0]
        batch_sz = batch_results.shape[0]
    
        for i in range(1, batch_sz):
            output = output + batch_results[i]
    
        output /= batch_sz
        best = np.argmax(output)
        best_choice = (label_list[best], output[best])
    
        nlabels = len(label_list)
        if nlabels > 2:
            output[best] = 0
            second_best = np.argmax(output)
            second_choice = (label_list[second_best], output[second_best])
        else:
            second_choice = (0, 0)
        
        result = (best_choice[0], best_choice[1], second_choice[0], second_choice[1])
            
        return pd.Series({'predAgeM2':result[0],'predAgeM2Prob':result[1],'predAgeM2Second':result[2],'predAgeM2SecondProb':result[3]})
    
    def getTrancheAge(self, x):
    
        def combinePredictionsAge(predM1, predM2, predM2Second, predM2Prob):

            TRANCHES = ['13-17', '18-24', '25-34', '35-44', '45-54', '55-64', '65+']

            def trancheAgeFromM1(a):
                if(a < 18):
                    return TRANCHES[0]
                if(a < 25):
                    return TRANCHES[1]
                if(a < 35):
                    return TRANCHES[2]
                if(a < 45):
                    return TRANCHES[3]
                if(a < 55):
                    return TRANCHES[4]
                if(a < 65):
                    return TRANCHES[5]
                return TRANCHES[6]

            if(predM2 in ['(0, 2)','(4, 6)','(8, 12)']):
                if(predM2Prob >= 0.4 or predM2Second in ['(15, 20)'] or predM1 < 18):
                    return TRANCHES[0]
                else:
                    return trancheAgeFromM1(predM1)
            if(predM2 in ['(15, 20)']):
                if(predM1 < 35 and predM1 >= 18):
                    return TRANCHES[1]
                if(predM1 < 18):
                    return TRANCHES[0]
            if(predM2 in ['(25, 32)', '(38, 43)']):
                if(predM1 < 25):
                    return TRANCHES[1]
                if(predM1 >= 25 and predM1 < 35):
                    return TRANCHES[2]
                if(predM1 >= 35):
                    return TRANCHES[3]
            if(predM2 in ['(48, 53)']):
                if(predM1 >= 40):
                    return TRANCHES[4]
                if(predM1 >= 55):
                    return TRANCHES[5]
            if(predM2 in ['(60, 100)']):
                if(predM1 >= 40 and predM1 < 65):
                    return TRANCHES[5]
                if(predM1 >= 65):
                    return TRANCHES[6]

            return 'UNK'

        return combinePredictionsAge(x['predAgeM1'], x['predAgeM2'], x['predAgeM2Second'], x['predAgeM2Prob'])
    
    def getGender(self, x):
        
        if(x['genderFN'] != 'UNK'):
            return x['genderFN']
    
        def combinePredictionsGender(predGenderM1, predGenderM2, predGenderM2Prob, predAgeM2, predAgeM2Second):

            if(predGenderM1 == predGenderM2):
                return predGenderM1

            if(predGenderM1 == 'M' and predGenderM2 == 'F' and predGenderM2Prob >= 0.92):
                return 'F'

            if(predAgeM2 in ['(0, 2)','(4, 6)'] or predAgeM2Second in ['(0, 2)','(4, 6)']):
                if(predGenderM1 == 'M' and predGenderM2 == 'F' and predGenderM2Prob >= 0.7):
                    return 'F'

            return predGenderM1

        return combinePredictionsGender(x['predGenderM1'], x['predGenderM2'], x['predGenderM2Prob'], x['predAgeM2'], x['predAgeM2Second'])
    
    def mergeGenderAndAge(self, row):
        if(row['trancheAge'] != 'UNK'):
            return row['gender'] + ' ' + row['trancheAge']
        return 'UNK'