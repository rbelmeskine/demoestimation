#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 09:05:27 2018

@author: rachid
"""

import pika
import json
import time
from dao.MongoInterface import MongoInterface
from dao.DfUtils import getDFUnknownGender
from detector.GenderDetectorFromFirstName import GenderDetectorFromFirstName
from detector.GenderDetectorFromImage import GenderDetectorFromImage
from conf import rabbitConf

class Consumer:
    def __init__(self):
        self.queueConf = {'name':'gender-estimator-v3'}
        self.mongoInterface = MongoInterface()
        self.detectorFN = GenderDetectorFromFirstName()
        self.detectorImg = GenderDetectorFromImage()
        
    def listen(self):
        #try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitConf['host'], rabbitConf['port']))
        channel = connection.channel()
        channel.queue_declare( self.queueConf['name'], # queue name 
                        durable=True)  # create automatically on reboot
                        #exclusive=False, #  we dont want it to be only accesible by queue creator
                        #auto_delete=False) # we dont want the queue to be deleted when last consumer detached
        channel.basic_qos(prefetch_count=1) # don't dispatch a new message to a worker until it has processed and acknowledged the previous one
        channel.basic_consume(self.consumeMsg,
                        queue=self.queueConf['name'],
                        no_ack=False)  #if set to false, message will stay on server till chan.basic_ack() called
        channel.start_consuming()
        channel.close()
        connection.clse()
        self.mongoInterface.close()
        '''except Exception:
            print("Something went wrong : ", Exception)'''
            
    def consumeMsg(self, ch, method, properties, body):
        msg = json.loads(body)
        source = msg['source']
        limit = int(msg['limit'])
        useImage = msg['useImage']
        print(source)
        print(limit)
        print(useImage)
        df = self.mongoInterface.getInstagramLikersWithLimit(False, limit)
        print(len(df))
        if(len(df) > 0):
            fullNameCol = ('full_name' if source == 'instagram' else 'name')
            df1 = self.detectorFN.generateGenderFromName(df, fullNameCol)
            print(len(df1))
            if(useImage):
                df2 = getDFUnknownGender(df1)
                df3 = self.detectorImg.generateGenderFromImage(df2, source)
                df4F = df3[df3['gender_from_image']=='F']
                df4M = df3[df3['gender_from_image']=='M']
                df1.loc[df4F.index.tolist(), 'gender'] = 'F'
                df1.loc[df4M.index.tolist(), 'gender'] = 'M'
            print(df1[['_id', 'full_name', 'gender']])
            self.mongoInterface.updateInstagramLikers(df1)
        time.sleep(30)
        print('FINISH...')
        ch.basic_ack(delivery_tag = method.delivery_tag)
        
consumer = Consumer()
consumer.listen()