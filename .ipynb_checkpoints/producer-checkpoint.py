#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 10:41:58 2018

@author: rachid
"""

import pika
import json

class Producer:
    def __init__(self):
        self.rabbitConf = {'host':'localhost',
                           'port':'5672',
                           'user':'admin',
                           'pass':'admin'}
        self.msgConf = {'limit':100,
                        'useImage':False}
        self.queueConf = {'name':'gender-estimator-v3'}
        
    def produce(self, source='instagram'):
        
        msg = {'source':source,
               'limit':self.msgConf['limit'],
               'useImage':self.msgConf['useImage']}
        
        #try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(self.rabbitConf['host'], self.rabbitConf['port']))
        channel = connection.channel()
        channel.queue_declare(self.queueConf['name'], # queue name 
                        durable=True)  # create automatically on reboot
                        #exclusive=False, #  we dont want it to be only accesible by queue creator
                        #auto_delete=False) # we dont want the queue to be deleted when last consumer detached
        channel.basic_publish(exchange='',
                        routing_key=self.queueConf['name'], 
                        body=json.dumps(msg),
                        properties=pika.BasicProperties(
                                delivery_mode = 2 # make message persistent
                                )
                        ) # if set to false, message will stay on server till chan.basic_ack() called

        channel.close()
        connection.close()
        
producer = Producer()
producer.produce()